TM01 Focus Punch
TM02 Dragon Claw
TM03 Water Pulse
TM04 Calm Mind
TM05 Roar
TM06 Toxic
TM07 Hail
TM08 Bulk Up
TM09 Bullet Seed
TM10 Hidden Power
TM11 Sunny Day
TM12 Taunt
TM13 Ice Beam
TM14 Blizzard
TM15 Hyper Beam
TM16 Light Screen
TM17 Protect
TM18 Rain Dance
TM19 Giga Drain
TM20 Safeguard
TM21 Frustration
TM22 Solar Beam
TM23 Iron Tail
TM24 Thunderbolt
TM25 Thunder
TM26 Earthquake
TM27 Return
TM28 Dig
TM29 Psychic
TM30 Shadow Ball
TM31 Brick Break
TM32 Double Team
TM33 Reflect
TM34 Shock Wave
TM35 Flamethrower
TM36 Sludge Bomb
TM37 Sandstorm
TM38 Fire Blast
TM39 Rock Tomb
TM40 Aerial Ace
TM41 Torment
TM42 Facade
TM43 Secret Power
TM44 Rest
TM45 Attract
TM46 Thief
TM47 Steel Wing
TM48 Skill Swap
TM49 Snatch
TM50 Overheat
TM51 Low Sweep
TM52 Focus Blast
TM53 Energy Ball
TM54 False Swipe
TM55 Scald
TM56 Fling
TM57 Charge Beam
TM58 Roost
TM59 Brutal Swing
TM60 Quash
TM61 Will O Wisp
TM62 Acrobatics
TM63 Embargo
TM64 Explosion
TM65 Shadow Claw
TM66 Payback
TM67 Smart Strike
TM68 Giga Impact
TM69 Rock Polish
TM70 Aurora Veil
TM71 Stone Edge
TM72 Volt Switch
TM73 Thunder Wave
TM74 Gyro Ball
TM75 Swords Dance
TM76 Fly
TM77 Psych Up
TM78 Bulldoze
TM79 Frost Breath
TM80 Rock Slide
TM81 X Scissor
TM82 Dragon Tail
TM83 Infestation
TM84 Poison Jab
TM85 Dream Eater
TM86 Grass Knot
TM87 Swagger
TM88 Sleep Talk
TM89 U Turn
TM90 Substitute
TM91 Flash Cannon
TM92 Trick Room
TM93 Wild Charge
TM94 Surf
TM95 Snarl
TM96 Nature Power
TM97 Dark Pulse
TM98 Waterfall
TM99 Dazzling Gleam
TM100 Confide
TM101 Sludge Wave
TM102 Liquidation
TM103 Leech Life
TM104 Play Rough
TM105 Psyshock
TM106 Aura Sphere
TM107 Power Gem
TM108 Earth Power
TM109 Gunk Shot
TM110 Stealth Rock
TM111 Heat Crash
TM112 Pollen Puff
TM113 Nasty Plot
TM114 Brave Bird
TM115 Bug Buzz
TM116 Dragon Dance
TM117 Blaze Kick
TM118 Tri Attack
TM119 Drain Punch
TM120 Mystical Fire