Hello!

Thanks for downloading Pokemon Chroma!

In order to play, you must patch the other file included with a clean Pokemon Emerald ROM.
I created my patch with FLIPS, which the .exe file has been included in this download for convenience.

If you have any bug reports or feature requests, feel free to join the official Discord! https://discord.gg/ajzpHYq

- vxo, Lead Developer

===========================================

Run flips.exe, click Apply Patch, select "pokechroma alpha.bps", then select "pokemon emerald.gba" and play the game using
a GBA emulator
